FROM php:7.2.5-apache
ARG APACHE_DOCUMENT_ROOT

ENV APACHE_DOCUMENT_ROOT "$APACHE_DOCUMENT_ROOT"
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN a2enmod rewrite
RUN apt-get update && apt-get install -y zlib1g-dev libpng-dev libmagickwand-dev default-mysql-client
RUN docker-php-ext-configure pdo_mysql && docker-php-ext-install pdo_mysql
RUN docker-php-ext-configure gd && docker-php-ext-install gd
RUN docker-php-ext-configure zip && docker-php-ext-install zip
RUN docker-php-ext-configure mbstring && docker-php-ext-install mbstring
RUN docker-php-ext-configure intl && docker-php-ext-install intl
RUN apt-get update \
    && rm -rf /var/lib/apt/lists/* \
    && pecl install imagick-beta \
    && echo "extension=imagick.so" > /usr/local/etc/php/conf.d/ext-imagick.ini

RUN touch /usr/local/etc/php/conf.d/uploads.ini
RUN echo "upload_max_filesize = 2G;" >> /usr/local/etc/php/conf.d/uploads.ini
RUN echo "post_max_size = 2G;" >> /usr/local/etc/php/conf.d/uploads.ini
RUN echo "memory_limit = 2G;" >> /usr/local/etc/php/conf.d/uploads.ini

WORKDIR /var/www